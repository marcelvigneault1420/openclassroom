<?php

namespace App\Events;

use Symfony\Component\EventDispatcher\Event;
use App\Entity\Ads;

class AdDeleted extends Event
{
	const NAME = 'ad.deleted';

	private $ad;

	public function __construct(Ads $ad)
	{
		$this->ad = $ad;
	}

	public function getAd()
	{
		return $this->ad;
	}
}