<?php

namespace App\Events;

use Symfony\Component\Security\Core\Event\AuthenticationEvent;
use App\Services\UserLastConnection;
use Psr\Log\LoggerInterface;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;

class LoginListener
{
	protected $userLastConnection;
	protected $logger;
	protected $em;

	public function __construct(UserLastConnection $userLastConnection, LoggerInterface $logger, EntityManagerInterface $entityManager)
	{
		$this->userLastConnection = $userLastConnection;
		$this->logger = $logger;
		$this->em = $entityManager;
	}

	public function onAuthenticationSuccess(AuthenticationEvent $event)
	{
		if ($event->getAuthenticationToken()->isAuthenticated())
		{
			$user = $event->getAuthenticationToken()->getUser();

			if($user instanceof User){
				$user = $this->userLastConnection->modifyLastConnection($user);
				$this->em->persist($user);
				$this->em->flush();
			}
		}
	}
}