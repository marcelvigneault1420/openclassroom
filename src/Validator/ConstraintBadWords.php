<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ConstraintBadWords extends Constraint
{
    public $message = 'The string "{{ string }}" contains an illegal character: it can only contain letters or numbers. Custom message: {{ custommessage }}';
    public $message2 = 'The string "{{ string }}" contains an illegal word: you can\'t post an ad with the word {{ badword }}. Your IP is {{ ip }}';
}