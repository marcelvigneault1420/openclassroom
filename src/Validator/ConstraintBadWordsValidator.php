<?php

namespace App\Validator;

use App\Entity\BadWords;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Exception\UnexpectedValueException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Psr\Log\LoggerInterface;

/**
 * @Annotation
 */
class ConstraintBadWordsValidator extends ConstraintValidator
{
    private $em;
    private $rs;
    private $cm;
    private $logger;

    public function __construct(RequestStack $rs, EntityManagerInterface $em, string $cm, LoggerInterface $logger)
    {
        $this->em = $em;
        $this->rs = $rs;
        $this->cm = $cm;
        $this->logger = $logger;
    }

    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof ConstraintBadWords) {
            throw new UnexpectedTypeException($constraint, ContainsAlphanumeric::class);
        }

        // custom constraints should ignore null and empty values to allow
        // other constraints (NotBlank, NotNull, etc.) take care of that
        if (null === $value || '' === $value) {
            return;
        }

        if (!is_string($value)) {
            // throw this exception if your validator cannot handle the passed type so that it can be marked as invalid
            throw new UnexpectedValueException($value, 'string');

            // separate multiple types using pipes
            // throw new UnexpectedValueException($value, 'string|int');
        }

        // Pour récupérer l'objet Request tel qu'on le connait, il faut utiliser getCurrentRequest du service request_stack
        $request = $this->rs->getCurrentRequest();

        // On récupère l'IP de celui qui poste
        $ip = $request->getClientIp();

        $this->logger->info('IP: ' . $ip);

        if (!preg_match('/^[a-zA-Z0-9]+$/', $value, $matches)) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ string }}', $value)
                ->setParameter('{{ custommessage }}', $this->cm)
                ->addViolation();
        }

        foreach ($this->em->getRepository(BadWords::class)->findAll() as $bw) {
            if (strpos($value, $bw->getWord()) !== false) {
                $this->context->buildViolation($constraint->message2)
                    ->setParameter('{{ string }}', $value)
                    ->setParameter('{{ badword }}', $bw->getWord())
                    ->setParameter('{{ ip }}', $ip)
                    ->addViolation();
            }
        }
    }
}
