<?php 
// src/Form/ArticleType.php
namespace App\Form;

use App\Entity\Ads;
use App\Entity\Comments;
use App\Entity\Categories;
use App\Form\AddressType;
use App\Form\TagsType;
use App\Form\ImageType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class AdsType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder->add('title', null, ['help' => 'This should be the title of your ad', 'required' => false])
			->add('description', null, ['attr' => ['maxlength' => 100]])
			->add('slug', null, ['label_format' => 'A %name%'])
			/*->add('comments', EntityType::class, [
				'class' => Comments::class,
				'expanded' => false,
				'multiple' => false,
				'placeholder' => 'Choose an option',
				'required' => true
			])*/
			->add('categories', EntityType::class, [
				'class' => Categories::class,
				'choice_label' => function ($cc) {
					return $cc->getDisplay();
				},
				'expanded' => true,
				'multiple' => true
			])
			->add('image', ImageType::class)
			->add('address', AddressType::class)
			->add('tags', CollectionType::class, [
				'entry_type' => TagsType::class,
				'allow_add' => true,
				'allow_delete' => true
			])
			->add('agreeTerms', CheckboxType::class, ['mapped' => false, 'required' => false])
			->add('NotLinkedDescription', TextType::class, ['mapped' => false])
			->add('isCheckedBox', CheckboxType::class, ['label' => 'Check this or bad will happen', 'required' => false,])
			;

	}

	public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Ads::class,
            // enable/disable CSRF protection for this form
            'csrf_protection' => true,
            // the name of the hidden HTML field that stores the token
            'csrf_field_name' => '_token',
            // an arbitrary string used to generate the value of the token
            // using a different string for each form improves its security
            'csrf_token_id'   => 'ads_type_csrf_string',
        ]);
    }
}