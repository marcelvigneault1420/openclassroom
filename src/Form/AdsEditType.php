<?php 
// src/Form/ArticleType.php
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class AdsEditType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder->remove('title')->remove('image');
	}

	public function getParent()
    {
    	return AdsType::class;
    }
}