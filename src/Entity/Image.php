<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ImageRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Image
{
    private function getUploadDir()
    {
        // On retourne le chemin relatif vers l'image pour un navigateur (relatif au répertoire /web donc)
        return __DIR__.'/../../public/uploads/img';
    }

    public function getFilePath()
    {
        return 'uploads/img/'.$this->getUrl();
    }

    // On ajoute cet attribut pour y stocker le nom du fichier temporairement
    private $tempFilename;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $url;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(min = 2, max = 200, minMessage="Your first name must be at least {{ limit }} characters", 
     * maxMessage="Your first name can't be more than {{ limit }} characters")
     */
    private $alt;

    /**
     * @Assert\Image()
     */
    private $file;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Ads", inversedBy="image", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $Ad;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getAlt(): ?string
    {
        return $this->alt;
    }

    public function setAlt(string $alt): self
    {
        $this->alt = $alt;

        return $this;
    }

    public function getAd(): ?Ads
    {
        return $this->Ad;
    }

    public function setAd(Ads $Ad): self
    {
        $this->Ad = $Ad;

        return $this;
    }

    public function getFile(): ?Ads
    {
        return $this->file;
    }

      // On modifie le setter de File, pour prendre en compte l'upload d'un fichier lorsqu'il en existe déjà un autre
    public function setFile(UploadedFile $file)
    {
        $this->file = $file;

        // On vérifie si on avait déjà un fichier pour cette entité
        if (null !== $this->url) {
            // On sauvegarde l'extension du fichier pour le supprimer plus tard, après que le persist ait vraiment fonctionné
            $this->tempFilename = $this->url;

            // On réinitialise les valeurs des attributs url et alt
            $this->url = null;
        }
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if ($this->file !== null)
        {
            $this->url = uniqid() . '.' . $this->file->guessExtension();
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function postUpload()
    {
        $this->removeOldFile();

        if ($this->file !== null)
        {
            $name = $this->file->getClientOriginalName();
            $this->file->move($this->getUploadDir(), $this->url);
        }
    }

    /**
     * @ORM\PreRemove()
     */
    public function preRemoveUpload()
    {
        // On sauvegarde temporairement le nom du fichier, car il dépend de l'id
        $this->tempFilename = $this->url;
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        $this->removeOldFile();
    }

    /* work with $this->url instead of $this->tempFilename */
    /* Wouldn't work if we would have needed the old ID because post the ID is back at 0 */
    public function removeOldFile()
    {
        if ($this->tempFilename !== null)
        {
            $oldFile = $this->getUploadDir().'/'.$this->tempFilename;
            if (file_exists($oldFile)) {
                unlink($oldFile);
            }
        }
    }
}
