<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Psr\Log\LoggerInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TagsRepository")
 */
class Tags
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Ads", inversedBy="tags", cascade={"persist"}, indexBy="name")
     */
    private $ad;

    public function __construct()
    {
        $this->ad = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Ads[]
     */
    public function getAd(): Collection
    {
        return $this->ad;
    }

    public function addAd(Ads $ad): self
    {
        if (!$this->ad->contains($ad)) {
            $this->ad[] = $ad;
        }

        return $this;
    }

    public function removeAd(Ads $ad): self
    {
        if ($this->ad->contains($ad)) {
            $this->ad->removeElement($ad);
        }

        return $this;
    }
}
