<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use App\Validator as MyAssert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AdsRepository")
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity(fields="description", message="Une annonce existe déjà avec ce titre.")
 */
class Ads
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\NotBlank
     * @Assert\Email(message="ENTER EMAIL CUSTOM MESSAGE")
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @Assert\NotBlank
     * @MyAssert\ConstraintBadWords
     * @Assert\Length(min = 3, max = 200, minMessage="Your first name must be at least {{ limit }} characters",
     * maxMessage="Your first name can't be more than {{ limit }} characters")
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date_created;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Comments", mappedBy="ad", orphanRemoval=true)
     */
    private $comments;

    /**
     * @Assert\Count(min="1", max="1")
     * @ORM\ManyToMany(targetEntity="App\Entity\Categories", inversedBy="ads")
     */
    private $categories;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_modified;

    /**
     * @Assert\Language
     * @Gedmo\Slug(fields={"title"})
     * @ORM\Column(type="string", length=128, nullable=true, unique=true)
     */
    private $slug;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ip_created;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Address", mappedBy="ad_id", cascade={"persist", "remove"})
     */
    private $address;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Tags", cascade={"persist"}, mappedBy="ad")
     */
    private $tags;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Image", mappedBy="Ad", cascade={"persist", "remove"})
     * @Assert\Valid
     */
    private $image;

    private $isCheckedBox;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="Ads")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @Assert\Callback()
     */
    public function isContentValid(ExecutionContextInterface $context)
    {
        $forbiddenWords = array('fuck', 'tabarnak');





        // On vérifie que le contenu ne contient pas l'un des mots
        if (preg_match('#' . implode('|', $forbiddenWords) . '#', $this->getTitle())) {
            // La règle est violée, on définit l'erreur
            $context
                ->buildViolation('Contenu invalide car il contient un mot interdit.') // message
                ->atPath('title')                                                   // attribut de l'objet qui est violé
                ->addViolation() // ceci déclenche l'erreur, ne l'oubliez pas
            ;


            $context
                ->buildViolation('BITCH.') // message
                ->atPath('slug')                                                   // attribut de l'objet qui est violé
                ->addViolation();
        }
    }

    /**
     * @Assert\IsTrue(message="ERROR DUDE")
     */
    public function isIsCheckedBox(): ? bool
    {
        return $this->isCheckedBox;
    }

    public function setIsCheckedBox(bool $isCheckedBox)
    {
        $this->isCheckedBox = $isCheckedBox;

        return $this;
    }

    /**
     *@ORM\PreUpdate
     */
    public function updateDateModified()
    {
        $this->setDateModified(new \DateTime());
    }

    public function __construct()
    {
        $this->comments = new ArrayCollection();
        $this->date_created = new \DateTime();
        $this->categories = new ArrayCollection();
        $this->tags = new ArrayCollection();
    }

    public function getId(): ? int
    {
        return $this->id;
    }

    public function getTitle(): ? string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ? string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getDateCreated(): ? \DateTimeInterface
    {
        return $this->date_created;
    }

    public function setDateCreated(\DateTimeInterface $date_created): self
    {
        $this->date_created = $date_created;

        return $this;
    }

    /**
     * @return Collection|Comments[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comments $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setAd($this);
        }

        return $this;
    }

    public function removeComment(Comments $comment): self
    {
        if ($this->comments->contains($comment)) {
            $this->comments->removeElement($comment);
            // set the owning side to null (unless already changed)
            if ($comment->getAd() === $this) {
                $comment->setAd(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Categories[]
     */
    public function getCategories(): Collection
    {
        return $this->categories;
    }

    public function addCategory(Categories $category): self
    {
        if (!$this->categories->contains($category)) {
            $this->categories[] = $category;
        }

        return $this;
    }

    public function removeCategory(Categories $category): self
    {
        if ($this->categories->contains($category)) {
            $this->categories->removeElement($category);
        }

        return $this;
    }

    public function getDateModified(): ? \DateTimeInterface
    {
        return $this->date_modified;
    }

    public function setDateModified(? \DateTimeInterface $date_modified): self
    {
        $this->date_modified = $date_modified;

        return $this;
    }

    public function getSlug(): ? string
    {
        return $this->slug;
    }

    public function setSlug(? string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getIpCreated(): ? string
    {
        return $this->ip_created;
    }

    public function setIpCreated(? string $ip_created): self
    {
        $this->ip_created = $ip_created;

        return $this;
    }

    public function getAddress(): ? Address
    {
        return $this->address;
    }

    public function setAddress(Address $address): self
    {
        $this->address = $address;

        // set the owning side of the relation if necessary
        if ($this !== $address->getAdId()) {
            $address->setAdId($this);
        }

        return $this;
    }

    /**
     * @return Collection|Tags[]
     */
    public function getTags(): Collection
    {
        return $this->tags;
    }

    public function addTag(Tags $tag): self
    {
        if (!$this->tags->contains($tag)) {
            $this->tags[] = $tag;
            $tag->addAd($this);
        }

        return $this;
    }

    public function removeTag(Tags $tag): self
    {
        if ($this->tags->contains($tag)) {
            $this->tags->removeElement($tag);
            $tag->removeAd($this);
        }

        return $this;
    }

    public function getImage(): ? Image
    {
        return $this->image;
    }

    public function setImage(Image $image): self
    {
        $this->image = $image;

        // set the owning side of the relation if necessary
        if ($this !== $image->getAd()) {
            $image->setAd($this);
        }

        return $this;
    }

    public function getUser(): ? User
    {
        return $this->user;
    }

    public function setUser(? User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
