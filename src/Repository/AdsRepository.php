<?php

namespace App\Repository;

use App\Entity\Ads;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Ads|null find($id, $lockMode = null, $lockVersion = null)
 * @method Ads|null findOneBy(array $criteria, array $orderBy = null)
 * @method Ads[]    findAll()
 * @method Ads[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AdsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Ads::class);
    }

    // /**
    //  * @return Ads[] Returns an array of Ads objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
/*
    public function findByLimitJointedToUserAndComments($offset, $limit)
    {
        return $this->createQueryBuilder('a')
            ->innerJoin('a.user', 'u')
            ->addSelect('u')
            ->orderBy('a.id', 'DESC')
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
    }
*/
    public function findAllBySearch($search)
    {
        $search = '%' . $search . '%';
        return $this->createQueryBuilder('a')
            ->andWhere('a.title LIKE :search') /* 'a.title = :search' */
            ->setParameter('search', $search)
            ->orderBy('a.id', 'DESC')
            ->getQuery()
            ->getResult();
    }
/*
    public function findAllByUser($user_id)
    {
        $em = $this->getEntityManager();

        $query = $em->createQuery('SELECT a FROM App\Entity\Ads a WHERE a.user = :user_id ORDER BY a.id DESC'
            )->setParameter('user_id', $user_id);

        return $query->getResult();
    }
*/
    public function whereCurrentYear(\Doctrine\ORM\QueryBuilder $qb)
    {
        $qb
        ->andWhere('a.date_created BETWEEN :start AND :end')
        ->setParameter('start', new \Datetime(date('Y').'-01-01'))  // Date entre le 1er janvier de cette année
        ->setParameter('end',   new \Datetime(date('Y').'-12-31'))  // Et le 31 décembre de cette année
        ;
    }
/*
    public function myFind()
    {
        $qb = $this->createQueryBuilder('a');

        // On peut ajouter ce qu'on veut avant
        $qb->where('a.user = :user')->setParameter('user', '1');

        // On applique notre condition sur le QueryBuilder
        $this->whereCurrentYear($qb);

        // On peut ajouter ce qu'on veut après
        $qb->orderBy('a.date_created', 'DESC');

        return $qb->getQuery()->getResult();
    }
*/
    public function getAdsWithCategories(array $cats)
    {
        $qb = $this->createQueryBuilder('a');

        $qb->innerJoin('a.categories', 'c')->addSelect('c');

        //$qb->where($qb->expr()->in('c.name', $cats));
        $qb->where($qb->expr()->in('c.name', ':cats'));
        $qb->setParameter('cats', $cats);

        return $qb->getQuery()->getResult();
    }

    /*
    public function findOneBySomeField($value): ?Ads
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
