<?php

namespace App\Services;

use Psr\Log\LoggerInterface;

class AntiSpam
{
    private $adminEmail;
    private $logs;

    public function __construct(LoggerInterface $logger)
    {
        $logger->info('AntiSpam constructed');
        $this->logs = $logger;
    }

    public function isSpam($text)
    {
        return strlen($text) < 50;
    }
}