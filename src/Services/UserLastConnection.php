<?php

namespace App\Services;

use App\Entity\User;

class UserLastConnection
{
	public function modifyLastConnection(User $user)
	{
	    $user->setLastConnection(new \DateTime);
	    return $user;
	}
}