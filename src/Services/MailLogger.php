<?php

namespace App\Services;

use App\Events\AdDeleted;
use Psr\Log\LoggerInterface;


class MailLogger
{
    private $adminEmail;
    private $logs;

    public function __construct($adminEmail, LoggerInterface $logger)
    {
        $this->adminEmail = $adminEmail;
        $logger->info('LOG 3 LOL');
        $this->logs = $logger;
        $this->logs->info('LOG 2 LOL');
    }


    public function SendEmailAdDeleted(AdDeleted $ad)
    {
    	$this->logs->info('LOG 4 LOL' . $ad->getAd()->getId());
    }
}