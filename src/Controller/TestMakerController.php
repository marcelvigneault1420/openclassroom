<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;

class TestMakerController extends AbstractController
{
    public function CustomIndex(Environment $twig)
    {
        $content = $twig->render('TestMaker/CustomIndex.html.twig', ['custom_var' => 'custom text sent']);
        return new Response($content);
    }
}
