<?php

namespace App\Controller;

//I have no ideas

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
//Routes annotations
use Symfony\Component\Routing\Annotation\Route;
//Get the url elements
use Symfony\Component\HttpFoundation\Request;
//Sessions
use Symfony\Component\HttpFoundation\Session\SessionInterface;

use Symfony\Component\EventDispatcher\EventDispatcherInterface;

use Symfony\Component\Form\FormError;

use App\Form\AdsType;
use App\Form\AdsEditType;

use Psr\Log\LoggerInterface;

use App\Entity\Ads;
use App\Entity\User;
use App\Entity\Tags;
use App\Entity\Comments;
use App\Events\AdDeleted;
use App\Services\AntiSpam;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
* @Route("/kijiji", name="kijiji_")
*/
class KijijiController extends AbstractController
{
    /**
     * @Route("/{_locale<en|fr>}/{page<\d+>}", defaults={"_locale": "en", "page": "1"}, name="index")
     */
    public function index($page, Request $request, SessionInterface $session, LoggerInterface $logger, AntiSpam $as)
    {
        $logger->info('LOG #1 LOL');



        if ($page < 1) {
            throw $this->createNotFoundException('Page "' . $page . '" inexistante.');
        }

        $search = $request->query->get('search');

        $adsRepository = $this->getDoctrine()->getRepository(Ads::class);
        //$listAds = $adsRepository->findByLimitJointedToUserAndComments(0, 5);
        //$listAds = $adsRepository->findAllBySearch('as');
        //$listAds = $adsRepository->findAllByUser($this->getDoctrine()->getRepository(Users::class)->find(1));
        //$listAds = $adsRepository->findAllByUser(1);
        /*
        $listAds = $adsRepository->findBy([
            'user' => 1, 'id' => '4'
            //'user' => $this->getDoctrine()->getRepository(Users::class)->find(1),
        ], ['id' => 'ASC']);
*/
        //$listAds = array($adsRepository->findOneByUser($this->getDoctrine()->getRepository(Users::class)->find(1)));
        //$listAds = $adsRepository->findByTitle('An ad title');
        //$listAds = $adsRepository->myFind();
        $listAds = $adsRepository->getAdsWithCategories(array('Cat3', 'Cat2'));

        return $this->render('kijiji/index.html.twig', ['search' => $search, 'listAds' => $listAds, 'nbAds' => count($listAds)]);
    }

    /**
     * @Route("/{_locale<en|fr>}/view/{id<\d+>}", defaults={"_locale": "en"}, name="view")
     */
    public function view($id)
    {
        if ($id > 0) {
            $ad = $this->getDoctrine()->getRepository(Ads::class)->findOneBy([
                'id' => $id
            ]);

            if ($ad) {
                $commentsCount = $this->getDoctrine()->getRepository(Comments::class)->countCommentsPerAd($ad);
                return $this->render('kijiji/view.html.twig', ['ad' => $ad, 'commentsCount' => $commentsCount]);
            }
        }

        return $this->redirectToRoute('kijiji_index');
    }

    /**
     * @Route("/{_locale<en|fr>}/add/{id<\d+>}", defaults={"_locale": "en", "id": "0"}, name="add")
     */
    public function add($id, Request $request, LoggerInterface $logger)
    {
        $em = $this->getDoctrine()->getManager();
        $form = null;

        if ($id > 0) {
            $ad = $em->getRepository(Ads::class)->find($id);

            if (!$ad) {
                $ad = new Ads();
            }

            $form = $this->createForm(AdsEditType::class, $ad);
        } else {
            $ad = new Ads();
            $form = $this->createForm(AdsType::class, $ad);
        }

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $ad = $form->getData();
            $agreeTerms = $form->get('agreeTerms')->getData();

            if ($ad->getTitle() == '123') {
                $error = new FormError("Please don't put 123 as a title");
                $form->get('title')->addError($error);
                $this->addFlash('error', 'Please fill all the fields');
            } else {
                if ($agreeTerms) {
                    $em = $this->getDoctrine()->getManager();
                    $user = $this->getDoctrine()->getRepository(User::class)->find(1);

                    //$ad->getImage()->upload($logger);

                    if ($user) {
                        $ad->setUser($user);
                        foreach ($ad->getTags() as $tag) {
                            if (($persistedTag = $em->getRepository(Tags::class)->findOneByName($tag->getName())) != null) {
                                $ad->removeTag($tag);

                                $tag = $persistedTag;
                            }
                            $tag->addAd($ad);
                            $em->persist($tag);
                            $em->flush();

                            $ad->addTag($tag);
                        }
                        $em->persist($ad);
                        $em->flush();

                        return $this->redirectToRoute('kijiji_view', ['id' => $ad->getId()]);
                    } else {
                        $this->addFlash('error', 'Unexpected error');
                    }
                } else {
                    $error = new FormError("Please agree the terms");
                    $form->get('agreeTerms')->addError($error);
                    $form->addError($error);
                    $this->addFlash('error', 'Please fill all the fields');
                }
            }
        } else {
            if ($form->isSubmitted() && !$form->isValid()) {
                $error = new FormError("There's errors in the form");
                $form->addError($error);
            }
        }

        return $this->render('kijiji/add.html.twig', array('form' => $form->createView()));
    }

    /**
     * @Route("/delete/{id<\d+>}", name="delete")
     *
     * @IsGranted("ROLE_ADMIN")
     */
    public function delete($id, Request $request, EventDispatcherInterface $edi)
    {
        $em = $this->getDoctrine()->getManager();
        $ad = $em->getRepository(Ads::class)->find($id);

        if ($ad) {
            $form = $this->get('form.factory')->create();

            //$event = new AdDeleted($ad);
            //$edi->dispatch(AdDeleted::NAME, $event);
            if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
                $em->remove($ad);
                $em->flush();

                $this->addFlash('info', 'Ad succefully deleted');

                return $this->redirectToRoute('kijiji_index');
            }

            return $this->render('kijiji/delete.html.twig', ['ad' => $ad, 'form' => $form->createView()]);
        }

        return $this->redirectToRoute('kijiji_index');
    }

    /**
     * @Route("/file/{filename}.{_format<txt|xml|jpg|jpeg>}", name="file")
     */
    public function getFile($filename, $_format)
    {
        return $this->render('../../public/files/' . $filename . '.' . $_format);
    }

    /**
     * @Route("/menu/{limit}", defaults={"limit": 4}, name="menu")
     */
    public function menu($limit)
    {
        $listAds = array(
            array('id' => 2, 'title' => 'Recherche ' . $limit . ' développeurs Symfony'),
            array('id' => 5, 'title' => 'Mission de webmaster'),
            array('id' => 9, 'title' => 'Offre de stage webdesigner')
        );

        return $this->render('kijiji/_menu.html.twig', ['ads' => $listAds]);
    }
}
