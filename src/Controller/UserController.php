<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Psr\Log\LoggerInterface;
use App\Entity\User;

//To ash
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserController extends AbstractController
{
	public function login(UserPasswordEncoderInterface $passwordEncoder)
	{
		$em = $this->getDoctrine()->getManager();
		$repo = $em->getRepository(User::class);

		$user = new User();
		$user->setEmail('myEmail@gmail.com');
		$user->setUsername('myUsername');
		$user->setPassword($passwordEncoder->encodePassword($user, 'myPassword'));
		$user->setLastConnection(new \DateTime);
		$em->persist($user);
        $em->flush();

        return $this->redirectToRoute('kijiji_index');
	}
}